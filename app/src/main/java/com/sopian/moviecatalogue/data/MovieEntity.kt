package com.sopian.moviecatalogue.data

import com.sopian.moviecatalogue.BuildConfig

data class MovieEntity(
    var id: Int,
    var title: String,
    var poster: String,
    var genre: List<Genre>?,
    var overview: String,
    var releaseDate: String,
    var voteAverage: Double,
    var runtime: Int?,
    var trailerUrl: String,
    var isMovie: Boolean = true
){

    data class Genre(
        var name: String
    )

    var imageUrl = BuildConfig.IMAGE_URL + poster
}
