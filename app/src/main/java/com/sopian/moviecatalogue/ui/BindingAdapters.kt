package com.sopian.moviecatalogue.ui

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.sopian.moviecatalogue.R
import com.sopian.moviecatalogue.data.MovieEntity
import java.text.ParseException
import java.text.SimpleDateFormat

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, imageUrl: String) {
    Glide.with(view)
        .load(imageUrl)
        .centerCrop()
        .transition(DrawableTransitionOptions.withCrossFade())
        .error(R.drawable.ic_loading)
        .into(view)
}

@BindingAdapter("mergeTextGenre")
fun bindMergeTextGenre(view: TextView, genres: List<MovieEntity.Genre>) {
    val builder = StringBuilder()

    if (genres.size > 1){
        for ((index, value) in genres.withIndex()){
            if (genres.lastIndex == index){
                builder.append(value.name)
            }else{
                builder.append(value.name)
                builder.append(" | ")
            }
        }
    }

    return view.setText(builder.toString())
}

@SuppressLint("SimpleDateFormat")
@BindingAdapter("formattedDate")
fun bindFormattedDate(view: TextView, date: String? = null) {
    date?.let {
        if (date.isEmpty()) {
            ""
        } else {
            val simpleDateFormat = SimpleDateFormat("dd, MMM yyyy")
            val simpleDateParseFormat = SimpleDateFormat("yyyy-MM-dd")
            try {
                val fdd = simpleDateFormat.format(simpleDateParseFormat.parse(date))
                view.setText(fdd)
            } catch (e: ParseException) {
                Log.w("date", e.message.toString())
            }
        }
    }
}

@BindingAdapter("formattedRuntime")
fun bindFormattedRuntime(view: TextView, runtime: Int){
    if (runtime == 0){
        view.visibility = View.GONE
    }

    val hour = runtime / 60
    val minute = runtime % 60

    if (runtime > 60){
        view.text = String.format(
            view.resources.getString(R.string.runtimeHourMinute), hour, minute)
    }else{
        if (runtime != 0){
            view.text = String.format(
                view.resources.getString(R.string.runtimeMinute), minute)
        }
    }
}