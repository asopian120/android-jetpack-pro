package com.sopian.moviecatalogue.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sopian.moviecatalogue.data.MovieEntity
import com.sopian.moviecatalogue.databinding.ItemsMovieBinding

class MoviesAdapter(
    private val listData: List<MovieEntity>,
    val onClick: (MovieEntity) -> Unit
) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsMovieBinding = ItemsMovieBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemsMovieBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = listData[position]
        holder.bind(movie)
    }

    override fun getItemCount(): Int = listData.size

    inner class ViewHolder(
        private val binding: ItemsMovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(movieEntity: MovieEntity){
            with(binding){
                data = movieEntity
                cardView.setOnClickListener { onClick(movieEntity) }
                posterCardView.setOnClickListener { onClick(movieEntity) }
            }
        }
    }
}