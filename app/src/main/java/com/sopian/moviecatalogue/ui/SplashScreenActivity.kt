package com.sopian.moviecatalogue.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sopian.moviecatalogue.R
import com.sopian.moviecatalogue.ui.home.HomeActivity
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Executors.newSingleThreadScheduledExecutor().schedule({
            processToMain()
        }, 2, TimeUnit.SECONDS)
    }

    private fun processToMain(){
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}