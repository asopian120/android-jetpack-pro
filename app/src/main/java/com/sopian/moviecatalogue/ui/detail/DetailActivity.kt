package com.sopian.moviecatalogue.ui.detail

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import com.google.android.material.appbar.MaterialToolbar
import com.sopian.moviecatalogue.R
import com.sopian.moviecatalogue.databinding.ActivityDetailBinding
import com.sopian.moviecatalogue.ui.home.HomeActivity
import com.sopian.moviecatalogue.utils.makeStatusBarTransparent
import com.sopian.moviecatalogue.utils.setMarginTop

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        makeStatusBarTransparent()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.view_container)) { _, insets ->
            findViewById<MaterialToolbar>(R.id.toolbar).setMarginTop(insets.systemWindowInsetTop)
            insets.consumeSystemWindowInsets()
        }

        with(binding){

            val viewModel: DetailViewModel by viewModels()
            val extras = intent.extras
            if (extras != null){
                val id = extras.getInt(EXTRA_ID)
                val isMovie = extras.getBoolean(EXTRA_IS_MOVIE)
                if (id != null){
                    viewModel.setSelectedMovie(id, isMovie)
                    val movie = viewModel.getDetailMovie()
                    data = movie
                    callback = Callback {
                        openYoutubeLink(it)
                    }
                }
            }

            var isToolbarShown = false
            detailScrollview.setOnScrollChangeListener(
                NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->
                    val shouldShowToolbar = scrollY >= toolbar.height

                    if (isToolbarShown != shouldShowToolbar) {
                        isToolbarShown = shouldShowToolbar
                        appbar.isActivated = shouldShowToolbar
                        toolbarLayout.isTitleEnabled = shouldShowToolbar
                    }
                }
            )

            toolbar.setNavigationOnClickListener {
                val intent = Intent(applicationContext, HomeActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun openYoutubeLink(youtubeID: String) {
        val intentApp = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + youtubeID))
        val intentBrowser = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + youtubeID))
        try {
            this.startActivity(intentApp)
        } catch (ex: ActivityNotFoundException) {
            this.startActivity(intentBrowser)
        }

    }

    fun interface Callback {
        fun open(link: String)
    }

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_IS_MOVIE = "extra_is_movie"
    }
}