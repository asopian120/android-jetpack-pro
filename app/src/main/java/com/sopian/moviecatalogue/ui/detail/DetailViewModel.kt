package com.sopian.moviecatalogue.ui.detail

import androidx.lifecycle.ViewModel
import com.sopian.moviecatalogue.data.MovieEntity
import com.sopian.moviecatalogue.utils.DataDummy

class DetailViewModel : ViewModel() {

    private var id: Int? = null
    private var isMovie: Boolean? = null

    fun setSelectedMovie(id: Int?, isMovie: Boolean?) {
        this.id = id
        this.isMovie = isMovie
    }

    fun getDetailMovie(): MovieEntity? {
        return id?.let { id ->
            isMovie?.let { isMovie ->
                DataDummy.getDetailMovie(id, isMovie)
            }
        }
    }
}