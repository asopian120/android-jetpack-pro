package com.sopian.moviecatalogue.ui.home

import androidx.lifecycle.ViewModel
import com.sopian.moviecatalogue.data.MovieEntity
import com.sopian.moviecatalogue.utils.DataDummy

class MoviesViewModel : ViewModel() {

    fun getMovies(isMovie: Boolean): List<MovieEntity> = DataDummy.generateDummyMovies(isMovie)
}