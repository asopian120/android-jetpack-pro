package com.sopian.moviecatalogue.utils

import com.sopian.moviecatalogue.data.MovieEntity

object DataDummy {

    fun generateDummyMovies(isMovie: Boolean): List<MovieEntity> {
        val movies = ArrayList<MovieEntity>()

        val genre1 = ArrayList<MovieEntity.Genre>()
        genre1.addAll(
            listOf(
                MovieEntity.Genre("Drama"),
                MovieEntity.Genre("Romance"),
                MovieEntity.Genre("Music")
            ),
        )

        val genre2 = ArrayList<MovieEntity.Genre>()
        genre2.addAll(
            listOf(
                MovieEntity.Genre("Action"),
                MovieEntity.Genre("Science Fiction"),
                MovieEntity.Genre("Adventure")
            )
        )

        val genre3 = ArrayList<MovieEntity.Genre>()
        genre3.addAll(
            listOf(
                MovieEntity.Genre("Animation"),
                MovieEntity.Genre("Fantasy"),
                MovieEntity.Genre("Family"),
            )
        )

        val genre4 = ArrayList<MovieEntity.Genre>()
        genre4.addAll(
            listOf(
                MovieEntity.Genre("Fantasy"),
                MovieEntity.Genre("Action"),
                MovieEntity.Genre("Adventure"),
            )
        )

        val genre5 = ArrayList<MovieEntity.Genre>()
        genre5.addAll(
            listOf(
                MovieEntity.Genre("Romance"),
                MovieEntity.Genre("Drama")
            )
        )

        val genre6 = ArrayList<MovieEntity.Genre>()
        genre6.addAll(
            listOf(
                MovieEntity.Genre("Thriller"),
                MovieEntity.Genre("Crime"),
                MovieEntity.Genre("Mystery")
            )
        )

        val genre7 = ArrayList<MovieEntity.Genre>()
        genre7.addAll(
            listOf(
                MovieEntity.Genre("Family"),
                MovieEntity.Genre("Fantasy"),
                MovieEntity.Genre("Comedy")
            )
        )

        val genre8 = ArrayList<MovieEntity.Genre>()
        genre8.addAll(
            listOf(
                MovieEntity.Genre("Action"),
                MovieEntity.Genre("Horror"),
                MovieEntity.Genre("Science Fiction")
            )
        )

        val genre9 = ArrayList<MovieEntity.Genre>()
        genre9.addAll(
            listOf(
                MovieEntity.Genre("Romance"),
                MovieEntity.Genre("Drama")
            )
        )

        val genre10 = ArrayList<MovieEntity.Genre>()
        genre10.addAll(
            listOf(
                MovieEntity.Genre("Thriller"),
                MovieEntity.Genre("Drama")
            )
        )


        val genre11 = ArrayList<MovieEntity.Genre>()
        genre11.addAll(
            listOf(
                MovieEntity.Genre("Crime"),
                MovieEntity.Genre("Action & Adventure")
            )
        )

        val genre12 = ArrayList<MovieEntity.Genre>()
        genre12.addAll(
            listOf(
                MovieEntity.Genre("Sci-Fi & Fantasy"),
                MovieEntity.Genre("Action"),
            )
        )

        val genre13 = ArrayList<MovieEntity.Genre>()
        genre13.addAll(
            listOf(
                MovieEntity.Genre("Sci-Fi & Fantasy"),
                MovieEntity.Genre("Crime"),
            )
        )

        val genre14 = ArrayList<MovieEntity.Genre>()
        genre14.addAll(
            listOf(
                MovieEntity.Genre("Drama")
            )
        )

        val genre18 = ArrayList<MovieEntity.Genre>()
        genre18.addAll(
            listOf(
                MovieEntity.Genre("Drama"),
                MovieEntity.Genre("Sci-Fi & Fantasy")
            )
        )

        movies.addAll(
            listOf(
                MovieEntity(
                    1,
                    "A star is born",
                    "/wrFpXMNBRj2PBiN4Z5kix51XaIZ.jpg",
                    genre1,
                    "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
                    "2018-10-03",
                    7.5,
                    136,
                    "nSbzyEJ8X9E",
                    true
                ),
                MovieEntity(
                    2,
                    "Alita: Battle Angel",
                    "/xRWht48C2V8XNfzvPehyClOvDni.jpg",
                    genre2,
                    "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.",
                    "2019-01-31",
                    7.1,
                    122,
                    "w7pYhpJaJW8",
                    true
                ),
                MovieEntity(
                    3,
                    "The Croods: A New Age",
                    "/tK1zy5BsCt1J4OzoDicXmr0UTFH.jpg",
                    genre3,
                    "After leaving their cave, the Croods encounter their biggest threat since leaving: another family called the Bettermans, who claim and show to be better and evolved. Grug grows suspicious of the Betterman parents, Phil and Hope, as they secretly plan to break up his daughter Eep with her loving boyfriend Guy to ensure that their daughter Dawn has a loving and smart partner to protect her.",
                    "2020-11-25",
                    8.1,
                    95,
                    "GkXeVIfbJOw",
                    true
                ),
                MovieEntity(
                    4,
                    "Wonder Woman 1984",
                    "/di1bCAfGoJ0BzNEavLsPyxQ2AaB.jpg",
                    genre4,
                    "Wonder Woman comes into conflict with the Soviet Union during the Cold War in the 1980s and finds a formidable foe by the name of the Cheetah.",
                    "2020-12-25",
                    7.1,
                    151,
                    "sfM7_JLk-84",
                    true
                ),
                MovieEntity(
                    5,
                    "After We Collided",
                    "/kiX7UYfOpYrMFSAGbI6j1pFkLzQ.jpg",
                    genre5,
                    "Tessa finds herself struggling with her complicated relationship with Hardin; she faces a dilemma that could change their lives forever.",
                    "2020-09-02",
                    7.3,
                    105,
                    "2SvwX3ux_-8",
                    true
                ),
                MovieEntity(
                    6,
                    "Wander",
                    "/2AwPvNHphpZBJDqjZKVuMAbvS0v.jpg",
                    genre6,
                    "After getting hired to probe a suspicious death in the small town of Wander, a mentally unstable private investigator becomes convinced the case is linked to the same 'conspiracy cover up' that caused the death of his daughter.",
                    "2020-12-04",
                    7.3,
                    94,
                    "mHc7z-Ks6dg",
                    true
                ),
                MovieEntity(
                    7,
                    "Godmothered",
                    "/80tDCErk6ymHS7YfvqJcbnnTtqa.jpg",
                    genre7,
                    "A young and unskilled fairy godmother that ventures out on her own to prove her worth by tracking down a young girl whose request for help was ignored. What she discovers is that the girl has now become a grown woman in need of something very different than a \"prince charming.\"",
                    "2020-12-04",
                    7.1,
                    113,
                    "KYWzEqX-J-4",
                    true
                ),
                MovieEntity(
                    8,
                    "The New Mutants",
                    "/xrI4EnZWftpo1B7tTvlMUXVOikd.jpg",
                    genre8,
                    "Five young mutants, just discovering their abilities while held in a secret facility against their will, fight to escape their past sins and save themselves.",
                    "2020-08-28",
                    6.4,
                    94,
                    "W_vJhUAOFpI",
                    true
                ),
                MovieEntity(
                    9,
                    "Ammonite",
                    "/5lx4pUHWZoOKJWsVsvurRRNW9FK.jpg",
                    genre9,
                    "In 1840s England, palaeontologist Mary Anning and a young woman sent by her husband to convalesce by the sea develop an intense relationship. Despite the chasm between their social spheres and personalities, Mary and Charlotte discover they can each offer what the other has been searching for: the realisation that they are not alone. It is the beginning of a passionate and all-consuming love affair that will defy all social bounds and alter the course of both lives irrevocably.",
                    "2020-11-13",
                    7.2,
                    118,
                    "P6l5d66n2Fk",
                    true
                ),
                MovieEntity(
                    10,
                    "Dreamland",
                    "/v8ax79K6TZEnMqSS5ePrNCnrK8R.jpg",
                    genre10,
                    "Amid the dust storms and economic depression of Dust Bowl Era Oklahoma, Eugene Evans finds his family farm on the brink of foreclosure. Discovering fugitive bank robber Allison Wells hiding in his small town, he is torn between claiming the bounty on her head and his growing attraction to the seductive criminal.",
                    "2020-11-13",
                    7.0,
                    98,
                    "agn4t4SOB_0",
                    true
                ),
                MovieEntity(
                    11,
                    "Arrow",
                    "/gKG5QGz5Ngf8fgWpBsWtlg5L2SF.jpg",
                    genre11,
                    "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.",
                    "2012-10-10",
                    6.5,
                    null,
                    "hTv13EjlLNg",
                    false
                ),
                MovieEntity(
                    12,
                    "The Mandalorian",
                    "/sWgBv7LV2PRoQgkxwlibdGXKz1S.jpg",
                    genre12,
                    "After the fall of the Galactic Empire, lawlessness has spread throughout the galaxy. A lone gunfighter makes his way through the outer reaches, earning his keep as a bounty hunter.",
                    "2019-11-12",
                    8.5,
                    null,
                    "aOC8E8z_ifw",
                    false
                ),
                MovieEntity(
                    13,
                    "Lucifer",
                    "/4EYPN5mVIhKLfxGruy7Dy41dTVn.jpg",
                    genre13,
                    "Bored and unhappy as the Lord of Hell, Lucifer Morningstar abandoned his throne and retired to Los Angeles, where he has teamed up with LAPD detective Chloe Decker to take down criminals. But the longer he's away from the underworld, the greater the threat that the worst of humanity could escape.",
                    "2016-01-25",
                    8.5,
                    null,
                    "X4bF_quwNtw",
                    false
                ),
                MovieEntity(
                    14,
                    "The Good Doctor",
                    "/6tfT03sGp9k4c0J3dypjrI8TSAI.jpg",
                    genre14,
                    "A young surgeon with Savant syndrome is recruited into the surgical unit of a prestigious hospital. The question will arise: can a person who doesn't have the ability to relate to people actually save their lives?",
                    "2017-09-25",
                    8.6,
                    null,
                    "fYlZDTru55g",
                    false
                ),
                MovieEntity(
                    15,
                    "Grey's Anatomy",
                    "/clnyhPqj1SNgpAdeSS6a6fwE6Bo.jpg",
                    genre14,
                    "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.",
                    "2005-03-27",
                    8.1,
                    null,
                    "cZcjNaAao6g",
                    false
                ),
                MovieEntity(
                    16,
                    "Industry",
                    "/4G2aJJs1lXoS0n6ftZglkXtZpc6.jpg",
                    genre14,
                    "In the cutthroat world of international finance, a group of young graduates compete for a limited set of permanent positions at a top investment bank in London. The boundaries between colleague, friend, lover, and enemy soon blur as they immerse themselves in a company culture defined as much by sex, drugs and ego as it is by deals and dividends.",
                    "2020-11-09",
                    7.0,
                    null,
                    "kI243_zwu3Y",
                    false
                ),
                MovieEntity(
                    17,
                    "Selena: The Series",
                    "/mYsWyfiIMxx4HDm0Wck7oJ9ckez.jpg",
                    genre14,
                    "As Mexican-American Tejano singer Selena comes of age and realizes her dreams, she and her family make tough choices to hold on to love and music.",
                    "2020-12-04",
                    7.4,
                    null,
                    "-0wz7n-h71M",
                    false
                ),
                MovieEntity(
                    18,
                    "His Dark Materials",
                    "/g6tIKGc3f1H5QMz1dcgCwADKpZ7.jpg",
                    genre18,
                    "Lyra is an orphan who lives in a parallel universe in which science, theology and magic are entwined. Lyra's search for a kidnapped friend uncovers a sinister plot involving stolen children, and turns into a quest to understand a mysterious phenomenon called Dust. She is later joined on her journey by Will, a boy who possesses a knife that can cut windows between worlds. As Lyra learns the truth about her parents and her prophesied destiny, the two young people are caught up in a war against celestial powers that ranges across many worlds.",
                    "2019-11-03",
                    8.1,
                    null,
                    "1yuIE1OYnVI",
                    false
                ),
                MovieEntity(
                    19,
                    "The Flash",
                    "/wHa6KOJAoNTFLFtp7wguUJKSnju.jpg",
                    genre18,
                    "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
                    "2014-10-07",
                    7.6,
                    null,
                    "Yj0l7iGKh8g",
                    false
                ),
                MovieEntity(
                    20,
                    "The Umbrella Academy",
                    "/scZlQQYnDVlnpxFTxaIv2g0BWnL.jpg",
                    genre18,
                    "A dysfunctional family of superheroes comes together to solve the mystery of their father's death, the threat of the apocalypse and more.",
                    "2019-02-15",
                    8.7,
                    null,
                    "0DAmWHxeoKw",
                    false
                )
            )
        )

        val filtered = movies.filter {
            it.isMovie == isMovie
        }

        return filtered
    }

    fun getDetailMovie(id: Int, isMovie: Boolean): MovieEntity {
        val movies = generateDummyMovies(isMovie)

        return movies.single {
            it.id == id && it.isMovie == isMovie
        }
    }
}