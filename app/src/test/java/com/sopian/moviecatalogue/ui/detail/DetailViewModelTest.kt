package com.sopian.moviecatalogue.ui.detail

import com.sopian.moviecatalogue.data.MovieEntity
import com.sopian.moviecatalogue.utils.DataDummy
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class DetailViewModelTest{

    private lateinit var viewModel: DetailViewModel

    @Before
    fun setUp(){
        viewModel = DetailViewModel()
    }

    @Test
    fun getMovie(){
        val isMovie = true
        val dummyMovie = DataDummy.generateDummyMovies(isMovie)[0]
        viewModel.setSelectedMovie(dummyMovie.id, isMovie)
        val movieEntity = viewModel.getDetailMovie()
        assertNotNull(movieEntity)
        assertEquals(dummyMovie.id, movieEntity?.id)
        assertEquals(dummyMovie.title, movieEntity?.title)
        assertEquals(dummyMovie.releaseDate, movieEntity?.releaseDate)
        assertEquals(dummyMovie.poster, movieEntity?.poster)
        assertEquals(dummyMovie.genre, movieEntity?.genre)
        assertEquals(dummyMovie.overview, movieEntity?.overview)
        assertEquals(dummyMovie.runtime, movieEntity?.runtime)
        assertEquals(dummyMovie.trailerUrl, movieEntity?.trailerUrl)
        assertEquals(dummyMovie.isMovie, movieEntity?.isMovie)
    }

    @Test
    fun getTvShow(){
        val isMovie = false
        val dummyMovie = DataDummy.generateDummyMovies(isMovie)[0]
        viewModel.setSelectedMovie(dummyMovie.id, isMovie)
        val movieEntity = viewModel.getDetailMovie()
        assertNotNull(movieEntity)
        assertEquals(dummyMovie.id, movieEntity?.id)
        assertEquals(dummyMovie.title, movieEntity?.title)
        assertEquals(dummyMovie.releaseDate, movieEntity?.releaseDate)
        assertEquals(dummyMovie.poster, movieEntity?.poster)
        assertEquals(dummyMovie.genre, movieEntity?.genre)
        assertEquals(dummyMovie.overview, movieEntity?.overview)
        assertEquals(dummyMovie.runtime, movieEntity?.runtime)
        assertEquals(dummyMovie.trailerUrl, movieEntity?.trailerUrl)
        assertEquals(dummyMovie.isMovie, movieEntity?.isMovie)
    }

    @Test
    fun getMovieWithEmptyData(){
        val isMovie = true
        val dummyMovie = dummyEmptyData()
        viewModel.setSelectedMovie(dummyMovie?.id, isMovie)
        val movieEntity = viewModel.getDetailMovie()
        assertNull(movieEntity)
    }

    @Test
    fun getTvShowWithEmptyData(){
        val isMovie = false
        val dummyMovie = dummyEmptyData()
        viewModel.setSelectedMovie(dummyMovie?.id, isMovie)
        val movieEntity = viewModel.getDetailMovie()
        assertNull(movieEntity)
    }

    private fun dummyEmptyData(): MovieEntity? {
        return null
    }
}