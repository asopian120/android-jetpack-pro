package com.sopian.moviecatalogue.ui.home

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MoviesViewModelTest {

    private lateinit var viewModel: MoviesViewModel

    @Before
    fun setUp(){
        viewModel = MoviesViewModel()
    }

    @Test
    fun getMovies(){
        val isMovie = true
        val movieEntities = viewModel.getMovies(isMovie)
        assertNotNull(movieEntities)
        assertEquals(10 , movieEntities.size)
    }

    @Test
    fun getTvShows(){
        val isMovie = false
        val movieEntities = viewModel.getMovies(isMovie)
        assertNotNull(movieEntities)
        assertEquals(10, movieEntities.size)
    }
}